- **Thématique :** Introduction Conception de Sites Web.

- **Notions liées :**  Interface web, le langage HTML de base.

- **Résumé de l’activité :** Activité de découverte de sites Web, comment ils sont fabriqués, utilisations et applications.

- **Objectifs :** Découverte du site Web. Saisir le code HTML : ajouter du texte, éditer du texte, des titres, insérer des images, éditer des images, notions de style et CSS.

- **Auteur :** Dziej, Leandro Edaurdo

- **Durée de l’activité :** 2hs

- **Forme de participation :** individuelle ou en gropue (de 2 ou 3 élèves), en autonomie, collective.

- **Matériel nécessaire :** Un ordinateur de groupe, éditeur de pages web (Visual Studio Code), navigateur Internet (Chrome, Firefox, etc.).

- **Préparation :** Aucune

- **Autres références :** https://fr.wikipedia.org/wiki/Conception_de_site_web                 
https://fr.wikipedia.org/wiki/Navigateur_web
