# Introduction Conceptionde Sites Web
- **Fiche de analyse d'intention de l'activité élève**
- **Auteur :** Dziej, Leandro Edaurdo
- **Fiche prof (lien) :** [fiche_prof_Introduction Conception de Sites Web Dziej.md](https://gitlab.com/-/ide/project/leandrodziej/mooc-2-ressources/tree/main/-/2.1_Penser%20-%20Concevoir%20-%20%C3%89laborer/fiche_prof_Introduction%20Conception%20de%20Sites%20Web%20Dziej.md/)

---

Après une présentation succincte de la notion de Site Web et de HTML (HyperText Markup Language), son historique et son application actuelle, on lance l'activité.
Les élèves formeront un groupe devant un ordinateur de groupe (de 2 ou 3 élèves). L'professeur,à son bureau, projettera son écran afin que les élèves puissent le recopier sur leur ordinateur si nécessaire.

## Etape 1 : Prendre connaissance de la thématique (15')

Tout d'abord, l'professeur expliquera à la classe que pour afficher des sites Web, ils doivent disposer d'un navigateur Web sur l'ordinateur (e.g. chrome, firefox, etc.). Ensuite, l'enseignant expliquera à la classe que pour créer ou modifier un site Web, ils ont besoin d'un éditeur de texte Web qui est généralement intégré dans un framework, comme code Visual Studio. Puis, Visual Studio Code est présenté en soulignant ses avantages et comment créer un projet et l'ouvrir avec Visual Studio Code.

_Notes pour le prof :_il est important de vérifier que les logiciels nécessaires sont déjà installés sur les ordinateurs.

## Etape 2 : Apprendre à créer un site web avec une structure simple et apprendre à utiliser les balises (45')

Alors, l'professeur explique la structure basique d'une page Web HTML5 (head, body, etc) et comment le HyperText Markup Language fonctionne avec des balises de base (e.g. h1 a h6, et p).
De plus, l'professeur expliquera que les balises sont généralement constituées d'une partie ouvrante et d'une partie fermante (e.g. \<h1>Ceci est un titre\</h1>\, \<p>Ceci est un paragraphe\</p>\, etc.), mais il existe également des balises sans contenu qui sont à la fois ouvrantes et fermantes (\<hr/>\, \<br/>\, etc.).

Enfin, l'professeur expliquera les attributs des balises et démontrera comment changer la couleur ou la taille du texte, comment ajouter des images et comment ajouter des liens hypertextes.

Regarder sur wikipédia les pages https://fr.wikipedia.org/wiki/Navigateur_web et https://fr.wikipedia.org/wiki/Conception_de_site_web.

_Notes pour le prof :_il est important que les élèves voient les effets sur un navigateur Web de la programmation avec le langage HTML.

## Etape 3 : Activité pratique. Créer un site web avec une structure simple à l'aide de balises, en groupe (60')

À ce stade, avec les informations acquises, les élèves recevront un guide pratique numérique conçu pour le professeur, dans le dossier de un projet existan, aussi conçu pour le professeur, pour réaliser et compléter le projet existant, avec un fichier inbdex.html, qui aura la structure pour réaliser l'activité pratique en groupe, alors les étudiants doivent:

- Répondre aux questions sur la structure de base de HTML5
- Corriger et compléter les lignes manquantes dans le fichier index.html (e.g. balises manquantes, etc.)
- Créer du code avec du texte, des images et des liens, avec certaines caractéristiques qui doivent figurer dans les attributs de l'étiquette

À ce stade les étudiants auront créé un site Web simple avec du texte, des images, et des hyperliens, avec des attributs spécifiques basés sur le guide pratique.

_Notes pour le prof :_il est important de consacrer le temps nécessaire pour que les étudiants comprennent et complètent les tâches proposées dans le guide pratique.

