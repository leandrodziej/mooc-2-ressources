# Introduction Conceptionde Sites Web
# Activité d'approfondissement de Sites Web
**Auteur :** Dziej, Leandro Edaurdo
- **Fiche prof (lien) :** [fiche_prof_Introduction Conception de Sites Web Dziej.md](https://gitlab.com/-/ide/project/leandrodziej/mooc-2-ressources/tree/main/-/2.1_Penser%20-%20Concevoir%20-%20%C3%89laborer/fiche_prof_Introduction%20Conception%20de%20Sites%20Web%20Dziej.md/)
- **Fiche de analyse d'intention de l'activité élève (lien) :** [Introduction Conception de Sites Web-fiche-analyse-intention-activité-élève-Dziej.md](https://gitlab.com/-/ide/project/leandrodziej/mooc-2-ressources/tree/main/-/2.2.%20Mettre%20en%20oeuvre%20-%20Animer/Introduction%20Conception%20de%20Sites%20Web-fiche-analyse-intention-activit%C3%A9-%C3%A9l%C3%A8ve-Dziej.md/)

**Contexte et la nature de la activité d'approfondissement**
Les activités d'approfondissement aident à consolider les concepts et les connaissances et à s'appuyer, supporter, sur les apprentissages antérieurs.
De plus, ces activités sont parfaitement adaptées à des contextes hétérogènes où certains groupes progressent plus vite que d'autres.
Aussi, ces activités peuvent être mises en œuvre sous forme de recherche par le groupe d'étudiants qui ont plus d'aisance ou sont plus motivés et contribuent ainsi à l'ensemble du cours.
Pour ce cas, une activité approfondie est proposée qui, basée sur ce qui a été appris précédemment, peut initier le groupe aux concepts de base du HTML, ou du web, sémantique et de l'accessibilité.

## Thématique de l'activité d'approfondissement de Sites Web

La structure sémantique améliorée d'une page Web. L'ajout dans la partie "body" des balises d'en-tête, \<header>C'est la partie d'en-tête\</header>\, de section, \<section>C'est la partie d'section\</section>\, d'article, \<article>C'est la partie d'article\</article>\, et de pied de page, \<footer>C'est la partie d'footer\</footer>\, permet une meilleure compréhension lors de l'analyse des métadonnées d'un site Web.

En outre, l'utilisation de l'attribut alt, par exemple dans la balise d'image, e.g. \<img src="imageHTML.png" alt="logo html">\, pour fournir des informations textuelles supplémentaires, qui peuvent être utilisées en cas d'échec du lien de l'image ou pour l'accessibilité aux lecteurs du site Web.

## Activité d'approfondissement de Sites Web (30')

Alors, l'professeur explique la structure de base d'une page Web HTML5 sémantique et les balises d'accessibilité de base, e.g.:

\<header>\</header>

\<section>\</section>

\<article>\</article>

\<footer>\</footer>

\<img alt="html image example" src="/assets_tutorials/img/image.jpg" />



Ensuite, l'enseignant demandera aux élèves de faire des recherches sur Internet sur ces sujets, le web sémantique et l'accessibilité web, et d'essayer d'améliorer un peu le site simple créé précédemment.


