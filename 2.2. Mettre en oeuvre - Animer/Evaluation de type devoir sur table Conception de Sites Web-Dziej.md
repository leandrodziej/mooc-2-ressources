# Introduction Conceptionde Sites Web
# Evaluation - Devoir sur table
**Auteur :** Dziej, Leandro Edaurdo
- **Fiche prof (lien) :** [fiche_prof_Introduction Conception de Sites Web Dziej.md](https://gitlab.com/-/ide/project/leandrodziej/mooc-2-ressources/tree/main/-/2.1_Penser%20-%20Concevoir%20-%20%C3%89laborer/fiche_prof_Introduction%20Conception%20de%20Sites%20Web%20Dziej.md/)
- **Fiche de analyse d'intention de l'activité élève (lien) :** [Introduction Conception de Sites Web-fiche-analyse-intention-activité-élève-Dziej.md](https://gitlab.com/-/ide/project/leandrodziej/mooc-2-ressources/tree/main/-/2.2.%20Mettre%20en%20oeuvre%20-%20Animer/Introduction%20Conception%20de%20Sites%20Web-fiche-analyse-intention-activit%C3%A9-%C3%A9l%C3%A8ve-Dziej.md/)

## Evaluation
Les critères d'évaluation seront communiqués préalablement aux élèves.

Cette évaluation se compose de trois parties différentes :

1 Activités en classe, en groupe, compte pour 50% de la note finale. Critères à évaluer : le processus en classe, résultat de la conception d'une page web de base, travail de groupe et collaboratif, autonomie.
2 Devoir sur table, examen écrit sur 10 points, compte pour 50% de la note finale (30')

Barème:
Il est noté de 1 à 10 (10 note maximum)
Chaque partie de l'évaluation (Partie 1 et 2) compte pour 50% de la note finale.

## Devoir sur table sur 10 points, compte pour 50% de la note finale (30')
(Cette partie est nécessaire aux étudiants pour qu'ils puissent réaliser l'évaluation)

**Données du élève:**
- **Nom et prénom:**
- **Carte national d'identité, nro. du document:**
- **Courrier électronique:**

**Consigne pour l'étudiant élève:**


Réponse, à propos de HTML de base simple, en sélectionnant une seule des options:

**1 La programmation concerne : (1 point)**

Une grande connaissance des mathématiques et de la physique

Créativité, collaboration et résolution de problèmes

Travail individuel et développement d'applications mobiles

**2 HTML est : (1 point)**

Un langage de balisage par balises

Un éditeur open source

Une application pour faire des sites web

**3 Un document HTML est structuré en : (1 point)**

head 

head et body

body

**4 Quelle balise utilisons-nous pour l'en-tête le plus important ? (1 point)**

\<h6>

\<h1>

\<heading>

\<head>

**5 Quelle balise utilisons-nous pour l'en-tête le plus important ? (1 point)**

\<h6>

\<h1>

\<heading>

\<head>

**6 Quel élément utilisons-nous pour générer un saut de ligne ? (1 point)**

\<br>

\<break>

\<hr>

**7 Comment lier un document CSS avec du HTML ? (1 point)** 

\<link type="estilos.css" rel="stylesheet">

\<link href="estilos.css" rel="stylesheet">

\<link src="estilos.css" rel="stylesheet">

**8 Que signifie l'attribut alt dans une image ? (1 point)**

Description et informations sur l'image

Décrivez le flux de l'image.

Décrivez la largeur et la hauteur de l'image

**9 Les listes ordonnées portent : (1 point)**

La balise <ul>

La balise <ol>

Ils n'ont pas d'étiquette

**10 Les listes non ordonnées portent : (1 point)**

La balise <ul>

La balise <ol>

Ils n'ont pas d'étiquette

